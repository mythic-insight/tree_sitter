defmodule Mix.Tasks.TreeSitter.Install do
  @moduledoc """
  Installs tree_sitter under `_build`.

  ```bash
  $ mix tree_sitter.install
  $ mix tree_sitter.install --if-missing
  ```

  By default, it installs #{TreeSitter.latest_version()} but you
  can configure it in your config files, such as:

      config :tree_sitter, :version, "#{TreeSitter.latest_version()}"

  ## Options

      * `--runtime-config` - load the runtime configuration
        before executing command

      * `--if-missing` - install only if the given version
        does not exist
  """

  @shortdoc "Installs tree_sitter under _build"
  @compile {:no_warn_undefined, Mix}

  use Mix.Task

  @impl true
  def run(args) do
    valid_options = [runtime_config: :boolean, if_missing: :boolean]

    case OptionParser.parse_head!(args, strict: valid_options) do
      {opts, []} ->
        if opts[:runtime_config], do: Mix.Task.run("app.config")

        if opts[:if_missing] && latest_version?() do
          :ok
        else
          if function_exported?(Mix, :ensure_application!, 1) do
            Mix.ensure_application!(:inets)
            Mix.ensure_application!(:ssl)
          end

          TreeSitter.install()
        end

      {_, _} ->
        Mix.raise("""
        Invalid arguments to tree_sitter.install, expected one of:

            mix tree_sitter.install
            mix tree_sitter.install --runtime-config
            mix tree_sitter.install --if-missing
        """)
    end
  end

  defp latest_version?() do
    version = TreeSitter.configured_version()
    match?({:ok, ^version}, TreeSitter.bin_version())
  end
end
