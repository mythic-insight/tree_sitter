defmodule Mix.Tasks.TreeSitter do
  @moduledoc """
  Invokes tree_sitter with the given args.

  Usage:

      $ mix tree_sitter TASK_OPTIONS PROFILE TREE_SITTER_ARGS

  Example:

      $ mix tree_sitter default assets/js/app.js --bundle --minify --target=es2016 --outdir=priv/static/assets

  If tree_sitter is not installed, it is automatically downloaded.
  Note the arguments given to this task will be appended
  to any configured arguments.

  ## Options

    * `--runtime-config` - load the runtime configuration
      before executing command

  Note flags to control this Mix task must be given before the
  profile:

      $ mix tree_sitter --runtime-config default assets/js/app.js

  """

  @shortdoc "Invokes tree_sitter with the profile and args"
  @compile {:no_warn_undefined, Mix}

  use Mix.Task

  @impl true
  def run(args) do
    switches = [runtime_config: :boolean]
    {opts, remaining_args} = OptionParser.parse_head!(args, switches: switches)

    if function_exported?(Mix, :ensure_application!, 1) do
      Mix.ensure_application!(:inets)
      Mix.ensure_application!(:ssl)
    end

    if opts[:runtime_config] do
      Mix.Task.run("app.config")
    else
      Application.ensure_all_started(:tree_sitter)
    end

    Mix.Task.reenable("tree_sitter")
    install_and_run(remaining_args)
  end

  defp install_and_run(args) do
    case TreeSitter.install_and_run(args) do
      0 -> :ok
      status -> Mix.raise("`mix tree_sitter #{Enum.join(args, " ")}` exited with #{status}")
    end
  end
end
