defmodule TreeSitter.MixProject do
  use Mix.Project

  @version "0.0.2"
  @source_url "https://gitlab.com/mythic-insight/tree_sitter"

  def project do
    [
      app: :tree_sitter,
      version: @version,
      elixir: "~> 1.11",
      deps: deps(),
      description: "Mix tasks for installing and invoking tree_sitter",
      package: [
        links: %{
          "GitHub" => @source_url,
          "tree_sitter" => "https://tree-sitter.github.io/tree-sitter/"
        },
        licenses: ["MIT"]
      ],
      docs: [
        main: "TreeSitter",
        source_url: @source_url,
        source_ref: "v#{@version}"
      ],
      aliases: [test: ["tree_sitter.install --if-missing", "test"]]
    ]
  end

  def application do
    [
      extra_applications: [:logger, inets: :optional, ssl: :optional],
      mod: {TreeSitter, []},
      env: [default: []]
    ]
  end

  defp deps do
    [
      {:castore, ">= 0.0.0"},
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false},
      {:yaml_elixir, "~> 2.9"}
    ]
  end
end
