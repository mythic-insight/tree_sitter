# TreeSitter

Mix tasks for installing and invoking [tree-sitter](https://tree-sitter.github.io/tree-sitter/).

## Installation

If you are going to build assets in production, then you add
`tree_sitter` as dependency on all environments but only start it
in dev:

```elixir
def deps do
  [
    {:tree_sitter, "~> 0.1.1"}
  ]
end
```

Once installed, change your `config/config.exs` and [pick a version
for the tree_sitter CLI](https://github.com/tree-sitter/tree-sitter/releases) of your choice:

```elixir
config :tree_sitter, version: "0.20.8"
```

Now you can install tree_sitter by running:

```bash
$ mix tree_sitter.install
```

The executable is kept at `_build/tree_sitter-TARGET`.
Where `TARGET` is your system target architecture.

# License

Copyright (c) 2023 Robert Prehn.

tree_sitter source code is licensed under the [MIT License](LICENSE.md).
