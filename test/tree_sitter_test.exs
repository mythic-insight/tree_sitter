defmodule TreeSitterTest do
  use ExUnit.Case, async: true

  @version TreeSitter.latest_version()

  test "runs" do
    assert ExUnit.CaptureIO.capture_io(fn ->
             assert TreeSitter.run(["--version"]) == 0
           end) =~ @version
  end

  test "updates on install" do
    Application.put_env(:tree_sitter, :version, "0.20.6")

    Mix.Task.rerun("tree_sitter.install", ["--if-missing"])

    assert ExUnit.CaptureIO.capture_io(fn ->
             assert TreeSitter.run(["--version"]) == 0
           end) =~ "0.20.6"

    Application.delete_env(:tree_sitter, :version)

    Mix.Task.rerun("tree_sitter.install", ["--if-missing"])

    assert ExUnit.CaptureIO.capture_io(fn ->
             assert TreeSitter.run(["--version"]) == 0
           end) =~ @version
  after
    Application.delete_env(:tree_sitter, :version)
  end
end
